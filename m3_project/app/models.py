# # -*- coding: utf-8 -*-
from django.db import models
from objectpack.ui import BaseEditWindow
from m3_ext.ui import all_components as ext


class windowaddu(BaseEditWindow):

      def _init_components(self):
          """
          Здесь следует инициализировать компоненты окна и складывать их в
          :attr:`self`.
          """
          super(windowaddu, self)._init_components()

          self.field__username = ext.ExtStringField(
              label=u'Логин',
              name='username',
              allow_blank=False,
              anchor='100%')

          self.field__email = ext.ExtStringField(
              label=u'E-Mail',
              name='email',
              allow_blank=False,
              anchor='100%')

          self.field__firstname = ext.ExtDateField(
              label=u'Имя',
              name='first_name',
              allow_blank=False,
              anchor='100%')

          self.field__lastname = ext.ExtDateField(
              label=u'Фамилия',
              name='last_name',
              allow_blank=False,
              anchor='100%')

      def _do_layout(self):
          """
          Здесь размещаем компоненты в окне
          """
          super(windowaddu, self)._do_layout()
          self.form.items.extend((
              self.field__username,
              self.field__email,
              self.field__firstname,
              self.field__lastname,
          ))

      def set_params(self, params):
          """
          Установка параметров окна

          :params: Словарь с параметрами, передается из пака
          """
          super(windowaddu, self).set_params(params)
          self.height = 'auto'
