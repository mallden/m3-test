# -*- coding: utf-8 -*-
from django.db import models
from objectpack.ui import BaseEditWindow
from m3_ext.ui import all_components as ext

# Окно добавления\редактирования для модели User
class windowaddu(BaseEditWindow):

      def _init_components(self):
          """
          Здесь следует инициализировать компоненты окна и складывать их в
          :attr:`self`.
          """
          super(windowaddu, self)._init_components()

          self.field__username = ext.ExtStringField(
              label=u'Логин',
              name='username',
              allow_blank=False,
              anchor='100%')

          self.field__pass = ext.ExtStringField(
              label=u'Пароль',
              name='password',
              allow_blank=False,
              anchor='100%')

          self.field__email = ext.ExtStringField(
              label=u'E-Mail',
              name='email',
              allow_blank=False,
              anchor='100%')

          self.field__firstname = ext.ExtStringField(
              label=u'Имя',
              name='first_name',
              allow_blank=False,
              anchor='100%')

          self.field__lastname = ext.ExtStringField(
              label=u'Фамилия',
              name='last_name',
              allow_blank=False,
              anchor='100%')

      def _do_layout(self):
          """
          Здесь размещаем компоненты в окне
          """
          super(windowaddu, self)._do_layout()
          self.form.items.extend((
              self.field__username,
              self.field__pass,
              self.field__email,
              self.field__firstname,
              self.field__lastname,
          ))

      def set_params(self, params):
          """
          Установка параметров окна

          :params: Словарь с параметрами, передается из пака
          """
          super(windowaddu, self).set_params(params)
          self.height = 'auto'

# Окно добавления\редактирования для модели Group
class windowaddg(BaseEditWindow):
     def _init_components(self):
         super(windowaddg, self)._init_components()

         self.field__name = ext.ExtStringField(
             label=u'Название',
             name='name',
             allow_blank=False,
             anchor='100%')

     def _do_layout(self):
         super(windowaddg, self)._do_layout()
         self.form.items.extend((
             self.field__name,
         ))

     def set_params(self, params):
         super(windowaddg, self).set_params(params)
         self.height = 'auto'

# Окно добавления\редактирования для модели Permission
class windowaddp(BaseEditWindow):

    def _init_components(self):
        super(windowaddp, self)._init_components()

        self.field__codename = ext.ExtStringField(
            label = u'CodeName',
            name = 'codename',
            allow_blank = False,
            anchor = '100%')

        self.field__name = ext.ExtStringField(
            label = u'Name',
            name = 'name',
            allow_blank = False,
            anchor = '100%')

    def _do_layout(self):
        super(windowaddp, self)._do_layout()
        self.form.items.extend((
            self.field__codename,
            self.field__name,
        ))

    def set_params(self,params):
        super(windowaddp, self).set_params(params)
        self.height = 'auto'

# Окно добавления\редактирования для модели ContentType
class windowaddct(BaseEditWindow):

    def _init_components(self):
        super(windowaddct, self)._init_components()

        self.field__applabel = ext.ExtStringField(
            label = u'AppLabel',
            name = 'app_label',
            allow_blank = False,
            anchor = '100%')

        self.field__model = ext.ExtStringField(
            label = u'Model',
            name = 'model',
            allow_blank = False,
            anchor = '100%')

    def _do_layout(self):
        super(windowaddct, self)._do_layout()
        self.form.items.extend((
            self.field__applabel,
            self.field__model,
        ))

    def set_params(self,params):
        super(windowaddct, self).set_params(params)
        self.height = 'auto'
