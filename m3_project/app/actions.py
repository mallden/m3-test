# # -*- coding: utf-8 -*-
from django.db import models
from objectpack.actions import ObjectPack
from . import ui
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
# from objectpack.ui import ModelEditWindow

class UserPack(ObjectPack):

    model = User
    add_window = ui.windowaddu
    edit_window = ui.windowaddu
    #add_window = edit_window = ModelEditWindow.fabricate(model)
    can_delete = True

    add_to_menu = True
    add_to_desktop = True

    columns = [
        {
            'data_index': 'username',
            'header': u'Логин',
            'width': 1,
        },
        {
            'data_index': 'password',
            'header': u'Пароль',
            'width': 1,
        },
        {
            'data_index': 'email',
            'header': u'E-mail',
            'width': 2,
        },
        {
            'data_index': 'first_name',
            'header': u'Имя',
            'width': 1,
        },
        {
            'data_index': 'last_name',
            'header': u'Фамилия',
            'width': 1,
        },
        {
            'data_index' : 'is_staff',
            'header' : u'Админ Права',
            'width' : 1,
        },
        {
            'data_index' : 'is_active',
            'header' : u'Активный',
            'width' : 1,
        },
        {
            'data_index' : 'date_joined',
            'header' : u'Дата регистрации',
            'width' : 1,
        }
        # {
        #   'data_index' : 'is_superuser',
        #   'header' : 'Супер юзер',
        #   'width' : 1,
        #  },
        # {
        #   'data_index' : 'last_login',
        #   'header' : 'Последний раз',
        #   'width' : 1,
        # }
    ]

class GroupPack(ObjectPack):

    model = Group
    add_window = edit_window = ui.windowaddg
    add_to_menu = True
    add_to_desktop = True

    columns = [
        {
            'data_index': 'name',
            'header': u'Название группы',
            'width': 2,
        }
    ]

class PermissionPack(ObjectPack):

    model = Permission
    add_window = edit_window = ui.windowaddp

    add_to_menu = True
    add_to_desktop = True

    columns = [
        {
            'data_index': 'content_type_id',
            'header': u'ContentTypeID',
            'width': 2,
        },
        {
            'data_index': 'codename',
            'header': u'CodeName',
            'width': 2,
        },
        {
            'data_index': 'name',
            'header': u'Name',
            'width': 2,
        }
    ]

class ContentTypePack(ObjectPack):

    model = ContentType
    add_window = edit_window = ui.windowaddct

    add_to_menu = True
    add_to_desktop = True

    columns = [
        {
            'data_index': 'app_label',
            'header': u'AppLabel',
            'width': 2,
        },
        {
            'data_index': 'model',
            'header': u'Model',
            'width': 2,
        }
    ]
