from django.conf.urls import url
from objectpack import desktop
from app.controller import controller
# from .actions import UserPack
# from .actions import GroupPack
# from .actions import PermissionPack
# from .actions import ContentTypePack
from . import actions

def register_urlpatterns():

   return [url(*controller.urlpattern)]


def register_actions():

   return controller.packs.extend([
    # YourActionPack()
    actions.ContentTypePack(),
    actions.UserPack(),
    actions.GroupPack(),
    actions.PermissionPack()
   ])

def register_desktop_menu():

   desktop.uificate_the_controller(
       controller,
       menu_root=desktop.MainMenu.SubMenu('Demo')
   )
