from setuptools import setup, find_packages
from os.path import join, dirname

def _read(file_name):
    with open(join(dirname(__file__), file_name)) as f:
        return f.read()


setup(
    name='m3_project',
    version='1.0.0',
    packages=['m3_project'],
    license='BSD',
    description=_read('DESCRIPTION'),
    author="Anatoly Yudin",
    author_email="doc34ru@gmail.com",
    classifiers=[
        'Intended Audience :: Developers',
        'Environment :: Web Environment',
        'Natural Language :: Russian',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'License :: OSI Approved :: BSD License',
        'Development Status :: 5 - Production/Stable',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.6',
        'Framework :: Django :: 1.4',
        'Framework :: Django :: 1.5',
        'Framework :: Django :: 1.6',
        'Framework :: Django :: 1.7',
        'Framework :: Django :: 1.8',
        'Framework :: Django :: 1.9',
        'Framework :: Django :: 1.10',
        'Framework :: Django :: 1.11',
        'Framework :: Django :: 2.0',
    ],
    include_package_data=True,
    long_description=_read('README'),
    install_requires=(
        'django==1.11',
	'm3-objectpack==2.2.25',
    ),
)
